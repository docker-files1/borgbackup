[![pipeline status](https://gitlab.com/docker-files1/borgbackup/badges/development/pipeline.svg)](https://gitlab.com/docker-files1/borgbackup/-/commits/development) ![Docker image pulls](https://img.shields.io/docker/pulls/oscarenzo/borgbackup) ![Docker image size](https://img.shields.io/docker/image-size/oscarenzo/borgbackup?sort=date) ![Project version](https://img.shields.io/gitlab/v/tag/docker-files1/borgbackup?label=version) ![Project license](https://img.shields.io/gitlab/license/docker-files1/borgbackup)

# borgbackup
This project work with borgbackup to create secure backups.

## 🧾 Components
 - Alpine
 - Borg 1.2.2
 - SSHFS
 - Python3-llfuse
 - logger

## ✍️ Advices
In case that use the `SSHFS` feature, the program ever will try to mount the volume as root and not regular user, so if you configure the variable `SSHFSUSER` with a value different to root, you should make sure that in the remote server you can run the follow command:

```
@borgman
/usr/bin/sudo /usr/lib/openssh/sftp-server
```

Here you can find an example of the rule:

```
borgman ALL=(root) NOPASSWD: /usr/lib/openssh/sftp-server
```

This method is for security purposes only, take in mind that if you want to use the `SSHFS` feature, you should make sure that has configured this variable **SSHFSHOST**, then you should make sure that this variables are not empty and are valids too:

- SSHFSPORT
- SSHFSUSER

❗ ***SSH private key*** authetication is the only one authentication method support and the *path* in the container where should be is ***/var/tmp/id_rsa***, so you should map your file here.

❗ Before to start to do backups using this **docker image**, you can prepare any volume as ***Borg repository***, you need to make sure that this is ready to use, if you have not a ***Borg repository*** created, you can start running this commands:

```
docker run -it --rm --name borg_container --env-file borgvars.yml -v /my_borg_desired_directory:/foo/bar borgbackup:latest prepare
```

This is an example for your vars file:

```
@borgvars.yml
BACKUP_ID=xxyyzz
BORG_REPO=/foo/bar
BORG_PASSPHRASE=xxyyzz
```

If you are using a remote directory using **sshfs**:

```
docker run -it --rm --name test_borg --privileged --env-file borgvars.yml -v /var/tmp/borgkey.priv:/var/tmp/id_rsa:ro -v /my_borg_desired_directory:/foo/bar oscarenzo/borgbackup:latest prepare
```

This is an example for your vars file:

```
@borgvars.yml
BACKUP_ID=xxyyzz
BORG_REPO=/foo/bar
BORG_PASSPHRASE=xxyyzz
SSHFSHOST=xxyyzz
SSHFSPORT=xxyyzz
SSHFSUSER=borgman
```


Following this [guide](#prepare) you can find additional information.

❗ For `LOGGING` the default behavior is that the container will send the logs to the ***logs*** directory on *Borg repository*

```
{BORG_REPO}/logs/borg.log
```

This file is the result of accumulate the contents written here:

```
/var/log/borg.log
```

This content is shown in realtime in *stdout* too.

In order to **keep healthy your server disk space**, is highly recommended that you configure your logrotate least this params:

```
{BORG_REPO}/logs/borg.log {
    weekly
    missingok
    rotate 10
    compress
    delaycompress
    notifempty
    olddir /var/log/borgbackup
    createolddir 0700 root root
}
```

You can decide to send the results report to *syslog server* to do that you need to the following variables:

- GRAYLOGHOST
- GRAYLOGPORT

As optional requirement, you can set too the *backup id* for identify the **origin** of the files then, for do that you need to set the variable (if not configured, the `hostname` of the container will be used instead):

- BACKUP_ID

Doing this you can receive a result message in your syslog server like this:

```
{Program: borgbackup, Id: Server, Backup: SUCCESS, Compact: SUCCESS, Prune: SUCCESS}
```

## ⚙️ Environment variables
This image uses environment variables to allow the configuration of some parameters at run time:

* Variable name: `BACKUP_ID`
* Default value: HOSTNAME
* Accepted values: String.
* Description: This is not mandatory variable, it will used for identify the backups in the Borg BORG repository.

----

* Variable name: `BORG_REPO`
* Default value: NULL
* Accepted values: Path.
* Description: This is a mandatory variable, it reference to the BORG repository, there are more information about it in Borg documentation https://borgbackup.readthedocs.io/en/stable/usage/general.html#environment-variables.

----

* Variable name: `BORG_PASSPHRASE`
* Default value: NULL
* Accepted values: String.
* Description: This is not mandatory variable, it reference to the BORG passphrase, there are more information about it in Borg documentation https://borgbackup.readthedocs.io/en/stable/usage/general.html#environment-variables.

----

* Variable name: `DIRECTORY`
* Default value: /mnt/content_directory_borg
* Accepted values: Path.
* Description: This is the path on the container where Borg will take the data to backup, if not configured */mnt/content_directory_borg* will be used.

----

* Variable name: `EXCLUDE`
* Default value: < dummy file >
* Accepted values: String, Regex.
* Description: This is not mandatory variable , it will tell to Borg the exclusion patterns that should be use when start the backup phase, can obtain more info about that running the command `borg help patterns` or using Borg documentation https://borgbackup.readthedocs.io/en/stable/usage/help.html#borg-patterns.

----

* Variable name: `BORG_BACKUP_NAME`
* Default value: NULL
* Accepted values: String.
* Description: This is mandatory only if use the `detail` option, it will tell to Borg that show the backup content details, if you do not set, the assistant will ask you the *backup name*.

----

* Variable name: `SSHFSHOST`
* Default value: NULL
* Accepted values: String.
* Description: This is not mandatory variable, it will use to configure a remote volume on the container, this is the SSHFS host address.

----

* Variable name: `SSHFSPORT`
* Default value: 22
* Accepted values: Int.
* Description: This is not mandatory variable, it will use to configure a remote volume on the container, this is the SSHFS port.

----

* Variable name: `SSHFSUSER`
* Default value: root
* Accepted values: String.
* Description: This is not mandatory variable, it will use to configure a remote volume on the container, this is the SSHFS user.

----

* Variable name: `GRAYLOGHOST`
* Default value: NULL
* Accepted values: String.
* Description: This is not mandatory variable, it will use to configure a remote log server where the backup routine results will be sended, this is the SYSLOG host.

----

* Variable name: `GRAYLOGPORT`
* Default value: 514
* Accepted values: Int.
* Description: This is not mandatory variable, it will use to configure a remote log server where the backup routine results will be sended, this is the SYSLOG port.

## 💁‍♂️ Actions
### Backup
This action will create a new backup using *BorgBackup*, you can call this process directly through the *entrypoint* running this command:

```
/var/tmp/docker-entrypoint.sh backup
```

After finish, if you has configured the variable `GRAYLOGHOST`, a result digest will be sended to the centralized log server, the message will look like this:

```
{Program: borgbackup, Backup: SUCCESS, COMPACT: SUCCESS, Prune: SUCCESS}
```

### Prepare
This action will prepare a new volume for you can use as *Borg* repository, you can call this process directly through the *entrypoint* running this command:

```
/var/tmp/docker-entrypoint.sh prepare
```

The assistant will show the **KEY** for you keep in safe together with your **BorgPassphrase**.

### List
This action will list all the available backups from your *Borg* repository, you can call this process directly through the *entrypoint* running this command:

```
/var/tmp/docker-entrypoint.sh list
```

The information can look like this:

```
Monday                               Mon, 2016-02-15 19:14:44
Tuesday                              Tue, 2016-02-16 19:15:11
```

### Detail
This action will show all the *files* and *folders* than contain a desired backup from your *Borg* repository, you can call this process directly through the *entrypoint* running this command:

```
/var/tmp/docker-entrypoint.sh detail
```

After run this command, you will asked for a **backup name**, using the last example result, you can use `Monday` as key and the result can look like this:

```
drwxr-xr-x user   group          0 Mon, 2016-02-15 18:22:30 home/user/Documents
-rw-r--r-- user   group       7961 Mon, 2016-02-15 18:22:30 home/user/Documents/Important.doc
```

### Mount
This action will mount a desired backup from your *Borg* repository on `temporary folder` for you can choose what *file*/*folder* want to **restore** or **watch**, you can call this process directly through the *entrypoint* running this command:

```
/var/tmp/docker-entrypoint.sh mount
```

After run this command, you will asked for a **backup name**, using the last example result, you can use `Monday` as key and the result can look like this:

```
tree /var/tmp/BorgBackupHolder

├── home/user/Documents
│   └── Important.doc
```

## 💬 Legend
* NKS => No key sensitive
* KS => Key sensitive
