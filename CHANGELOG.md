# [1.9.1](/Docker_Files/borgbackup/compare/1.9.1...master)
* If the source file's private key does not have the correct permissions, trapping the file may result in failure. Now, this process ensures that the trapped file has the correct permissions.
* Little improvements in the documentation

# [1.9.0](/Docker_Files/borgbackup/compare/1.9.0...master)
* Update documentation with pending information about the new features and *SSHFS method* related
* New variable for identify the origin backed up files, `BACKUP_ID`

# [1.8.0](/Docker_Files/borgbackup/compare/1.8.0...master)
* Improvement in order to catch correctly the log messages while the program is running
* Adding new package in the `Dockefile`, the package is *logger* for give support to **graylog*, issue #15
* New featured added, nos is possible:
    -   List available backups, using the options `list`
    -   Show details of desired backup, using the options `detail`
    -   Mount desired backup on another directory temporary, using the options `mount`
    -   Prepare new volumes to use it with *Borg*, using the options `prepare`
* Adding verbose on all the executed command for improve the controls
* Update the documentation with the *new features*

# [1.7.0](/Docker_Files/borgbackup/compare/1.7.0...master)
* Improvement in order to catch correctly the log messages while the program is running

# [1.6.0](/Docker_Files/borgbackup/compare/1.6.0...master)
* CICD migraton from Jenkins to GitlabCI

# [1.5.1](/Docker_Files/borgbackup/compare/1.5.1...master)
* `borg_logging` function use tee command of bad way provoking that the process never finish

# [1.5.0](/Docker_Files/borgbackup/compare/1.5.0...master)
* Docker linter apply and little hotfix that might affect to some files that match with the current `.gitignore`
* Some fixes and information added to `README.md` file
* Environment variable `SSHFSUSERIDENTITY` removed for avoid confusion, is an innecesary parameter.
* Optimize the function *borg_logging*

# [1.4.0](/Docker_Files/borgbackup/compare/1.4.0...master)
* Se actualiza el formato de la documentación

# [1.3.1](/Docker_Files/borgbackup/compare/1.3.1...master)
* Se corrige bug al interpretar las regex para las exclusiones

# [1.3.0](/Docker_Files/borgbackup/compare/1.3.0...master)
* Se habilita el logado de las ejecuciones de *Borg*, éstas por defecto se guardaran en el directorio `logs` dentro del repositorio.
* Se elimina la variable de entorno `NODENAME` para usar el *placeholder* disponible en `Borg`

# [1.2.2](/Docker_Files/borgbackup/compare/1.2.2...master)
* Corrección de enlaces de comparación de commits en el *CHANGELOG.md*

# [1.2.1](/Docker_Files/borgbackup/compare/1.2.1...master)
* Se aplica una mejora en la gestión de los errores a fin de que el script pueda terminar completamente

# [1.2.0](/Docker_Files/borgbackup/compare/1.2.0...master)
* Trap the private key to borgbackup for can be deleted inmediatly on the host, for **security purposes**
    * Sorting the tasks related to the feature *SSHFS*

# [1.1.0](/Docker_Files/borgbackup/compare/1.1.0...master)
* Se agrega el paquete < py3-llfuse >, el cual hace posible poder usar la característica **mount** de Borg para poder navegar entre los archivos de *backup*
* Se agrega la variable de entorno `EXCLUDE`, la cual permite pasarle a *Borg* un patrón de exclusión al momento de realizar los *backups*

# [1.0.0](/Docker_Files/borgbackup/compare/1.0.0...master)
* Primera versión del docker file `borgbackup` basado en Alpine linux.
