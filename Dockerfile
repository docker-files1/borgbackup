FROM alpine:3.17

LABEL DistBase="Alpine 3.17"

RUN apk update && \
	apk add --no-cache --upgrade apk-tools && \
	apk upgrade --available && \
	apk add --no-cache bash borgbackup sshfs \
	py3-llfuse logger

# Syntax-check software pre-configurations
COPY scripts/docker-entrypoint.sh /var/tmp/
COPY config/logging.conf /var/tmp/

RUN chmod +x /var/tmp/docker-entrypoint.sh

ENTRYPOINT ["/var/tmp/docker-entrypoint.sh"]

CMD ["backup"]
# docker build -t oscarenzo/borgbackup:latest .
# docker buildx build --push -t oscarenzo/borgbackup:latest --platform linux/amd64,linux/arm64/v8 .
# docker buildx build --push -t nexus3.services.arroyof.com:8086/borgbackup:1.x.x --platform linux/amd64 .
